18def divisors(num):
  return sum(list(i for i in range(1,num) if num % i == 0))

def Amicable_numbers(num):
  return list((i,j) for i in range(num) for j in range(num) if divisors(i) == j and divisors(j) == i and i > j)

