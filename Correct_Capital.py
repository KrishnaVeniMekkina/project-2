def detectCapitalUse(word):
    return word.isupper() or word.islower() or word == word.capitalize()
