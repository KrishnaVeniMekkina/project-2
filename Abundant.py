import math
from itertools import product as prod 
ABUNDANT_LIMIT = 28123
def lo_divisors(n):
    LIMIT - int(math.sqrt(n))
    return [f for f in range(2, LIMIT+1) if n%f == 0]
def hi_divisors(n):
    return [n//f for f in lo_divisors(n)]
def all_divisors(n):
    return set([1] + lo_divisors(n) + hi_livisors(n))
