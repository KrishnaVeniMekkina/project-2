
from itertools import permutations as npr

def is_prime(s):
  n = int(s)
  if n in [2,3,5,7]:
    return True
  if n % 2 == 0:
    return False
  r = 3
  while r*r <= n:
    if n % r == 0:
      return False
    r += 2
  return True

def permutations(s):
  return [''.join(x) for x in npr(s)]

def circular(s):
  return [x for x in permutations(s) if s in x + x]

def is_circular_prime(s):
  return all([is_prime(x) for x in circular(s)])

def ranger(LIMIT):
  return len([str(i) for i in range(2,LIMIT) if is_circular_prime(str(i)) == True])

print(ranger(100))
