from itertools import permutations as npr
from sympy import isprime

def permutations(s):
  return [''.join(x) for x in npr(s)]

def circular(s):
  return [x for x in permutations(s) if s in x + x]

def is_circular_prime(s):
  return all([isprime(x) for x in circular(s)])

def ranger(LIMIT):
  return len([str(i) for i in range(0,LIMIT) if is_circular_prime(str(i)) == True])
